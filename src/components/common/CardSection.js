import React from 'react';
import {View, StyleSheet} from 'react-native';

const CardSection = ({children}) => (
    <View style={styles.CardSection}>{children}</View>
);

const styles = StyleSheet.create({
    CardSection: {
        borderBottomWidth: 1,
        borderColor: '#ddd',
        padding: 5,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        position: 'relative'
    }
});

export {CardSection};