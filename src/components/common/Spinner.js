import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';

const Spinner = ({size}) => (
    <View style={styles.Spinner}>
        <ActivityIndicator size={size || 'large'} />
    </View>
);

const styles = StyleSheet.create({
    Spinner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export {Spinner};