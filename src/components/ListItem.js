import React, {Component} from 'react';
import {Text, TouchableWithoutFeedback, View, Platform, UIManager, LayoutAnimation,StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {CardSection} from './common';
import {selectLibrary} from '../actions';

if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

class ListItem extends Component {
    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    renderDetail() {
        if (this.props.expanded) {
            return (
                <CardSection>
                    <Text>{this.props.description}</Text>
                </CardSection>
            );
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(this.props.id)}>
                <View>
                    <CardSection style={styles.ListItem}>
                        <Text style={styles.title}>{this.props.title}</Text>
                    </CardSection>
                    {this.renderDetail()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
};

const styles = StyleSheet.create({
    ListItem: {},
    title: {fontSize: 18, paddingLeft: 15},
    description: {
    }
});

const mapStateToProps = ({selection}, {id}) => ({expanded: (selection === id)});
export default connect(mapStateToProps, {selectLibrary})(ListItem);