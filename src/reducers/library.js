import libraryList from './libraryList.json';

export default (state = libraryList, action) => {
    switch (action.type) {
        default: return state;
    }
}